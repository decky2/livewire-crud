@livewireStyles
@livewireScripts
<div>
    <div class="flex items-center justify-center h-full">
        <div x-data="{ showModalAdd: false, showModalUpdate: false}">
            <!-- Button to open the modal -->
            <div x-show="showModalAdd" class="fixed inset-0 transition-opacity" aria-hidden="true" @click="showModalAdd = false">
                <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
            </div>
            <div x-show="showModalUpdate" class="fixed inset-0 transition-opacity" aria-hidden="true" @click="showModalUpdate= false">
                <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
            </div>
            <div class="container" style="width: 1400px">
                {{-- Data Mahasiswa Section --}}
                <div class="p-5">
                    <div class="w-full flex justify-center align-center py-3">
                        <h1 class="mr-3 font-semibold text-2xl text-dark">Data Mahasiswa</h1>
                    </div>
                    {{-- Data Table --}}
                    <table class="w-full my-2 align-middle text-dark border-neutral-200=">
                        <thead class="align-bottom">
                            <tr class="font-semibold text-[0.95rem] text-secondary-dark ">
                                <th class="pb-3 text-center min-w-[100px]">Foto</th>
                                <th class="pb-3 text-center min-w-[100px]">Nama</th>
                                <th class="pb-3 text-center min-w-[100px]">NIM</th>
                                <th class="pb-3 text-center min-w-[100px]">Kelamin</th>
                                <th class="pb-3 text-center min-w-[100px]">Tempat Lahir</th>
                                <th class="pb-3 text-center min-w-[150px]">Tanggal Lahir</th>
                                <th class="pb-3 text-center min-w-[100px]">Email</th>
                                <th class="pb-3 text-center min-w-[100px]">Nomor Telepon</th>
                                <th class="pb-3 text-center min-w-[100px]">Alamat</th>
                                <th class="pb-3 text-center min-w-[100px]">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            {{-- Data Mahasiswa Loop --}}
                            @forelse ($mahasiswas as $mahasiswa)
                            <tr class="border-1 border-b border-dashed last:border-b-2">
                                <td class="p-3 pl-0">
                                    <div class="relative inline-block shrink-0 rounded-2xl me-3">
                                        <img src="{{ asset('storage/'.$mahasiswa->foto_profil) }}"  class="w-[100px] h-[100px] inline-block shrink-0 rounded-2xl" alt="">   
                                    </div>
                                </td>
                                <td class="p-3 pr-0 text-center">{{ $mahasiswa->nama_lengkap }}</td>
                                <td class="p-3 pr-0 text-center">{{ $mahasiswa->nim }}</td>
                                @if ($mahasiswa->jenis_kelamin == 'L')
                                    <td>Laki-Laki</td>
                                @endif
                                @if ($mahasiswa->jenis_kelamin == 'P')
                                    <td>Perempuan</td>
                                @endif
                                <td class="p-3 pr-0 text-center">{{ $mahasiswa->tempat_lahir }}</td>
                                <td class="p-3 pr-0 text-center">{{ $mahasiswa->tanggal_lahir }}</td>
                                <td class="p-3 pr-0 text-center">{{ $mahasiswa->email }}</td>
                                <td class="p-3 pr-0 text-center">{{ $mahasiswa->nomor_telepon }}</td>
                                <td class="p-3 pr-0 text-center">{{ $mahasiswa->alamat_lengkap }}</td>
                                {{-- Edit and Delete Buttons --}}
                                <td class="px-6 py-4">
                                    <div class="flex justify-end gap-4">
                                    <a x-data="{ tooltip: 'Delete' }" href="#" type="button" wire:click="delete({{ $mahasiswa->id }})"">
                                        <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            fill="none"
                                            viewBox="0 0 24 24"
                                            stroke-width="1.5"
                                            stroke="currentColor"
                                            class="h-6 w-6"
                                            x-tooltip="tooltip"
                                        >
                                            <path
                                            stroke-linecap="round"
                                            stroke-linejoin="round"
                                            d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0"
                                            />
                                        </svg>
                                    </a>
                                    <a x-data="{ tooltip: 'Edite' }" href="#" type="button" @click="showModalUpdate = true" wire:click="edit({{$mahasiswa->id}})">
                                        <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            fill="none"
                                            viewBox="0 0 24 24"
                                            stroke-width="1.5"
                                            stroke="currentColor"
                                            class="h-6 w-6"
                                            x-tooltip="tooltip"
                                        >
                                            <path
                                            stroke-linecap="round"
                                            stroke-linejoin="round"
                                            d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L6.832 19.82a4.5 4.5 0 01-1.897 1.13l-2.685.8.8-2.685a4.5 4.5 0 011.13-1.897L16.863 4.487zm0 0L19.5 7.125"
                                            />
                                        </svg>
                                    </a>
                                </div>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="10" align="center" >
                                    No Data Mahasiswa Found.
                                </td>
                            </tr>
                            
                            @endforelse
                        </tbody>
                    </table>
                    {{-- Add Data Mahasiswa Button --}}
                    <button wire:click="create()" class="w-full my-2 py-2 bg-blue-600 text-white rounded-lg" @click="showModalAdd = true">Add Data Mahasiswa</button>
                </div>
            </div>
    
            <!-- ModalAdd -->
            <div x-show="showModalAdd" x-transition:enter="transition ease-out duration-300 transform" x-transition:enter-start="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95" x-transition:enter-end="opacity-100 translate-y-0 sm:scale-100" x-transition:leave="transition ease-in duration-200 transform" x-transition:leave-start="opacity-100 translate-y-0 sm:scale-100" x-transition:leave-end="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95" class="fixed z-10 inset-0 overflow-y-auto" x-cloak>
                <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
                    <!-- Modal panel -->
                    <div class="w-full inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full" role="dialog" aria-modal="true" aria-labelledby="modal-headline">
                        <div class="my-3 p-3 bg-body rounded shadow-sm">
                            <!-- Formulir dengan Encoding Type untuk Mengunggah Berkas -->
                            <form enctype="multipart/form-data" wire:submit="store">
                                <!-- Field Nama -->
                                <div class="w-full flex flex-row gap-5 my-3 items-center">
                                    <label for="nama" class="block text-sm font-medium text-gray-900 w-[100px]">Nama</label>
                                    <div class="w-full flex flex-col gap-1">
                                        <input type="text" class="block w-full text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer p-2.5" wire:model="nama_lengkap">
                                        @error('nama_lengkap') <span class="text-danger">{{ $message }}</span>@enderror
                                    </div>
                                </div>
                                <!-- Field NIM -->
                                <div class="w-full flex flex-row gap-5 my-3 items-center">
                                    <label for="nim" class="block text-sm font-medium text-gray-900 w-[100px]">NIM</label>
                                    <div class="w-full flex flex-col gap-1">
                                        <input type="text" class="block w-full text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer p-2.5" wire:model="nim">
                                            @error('nim')
                                            @if ($message === 'The nim has already been taken.')
                                                <span class="text-danger">NIM sudah ada.</span>
                                            @else
                                                <span class="text-danger">{{ $message }}</span>
                                            @endif
                                        @enderror 
                                    </div>
                                </div>
                                <!-- Field Jenis Kelamin-->
                                <div class="w-full flex flex-row gap-5 my-3 items-center">
                                    <label for="jenis_kelamin" class="block text-sm font-medium text-gray-900 w-[100px]">Jenis Kelamin</label>
                                    <div class="w-full flex flex-col gap-1">
                                        <select id="jenis_kelamin" class="block w-full text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer p-2.5" wire:model="jenis_kelamin">
                                            <option selected value="">Pilih Jenis Kelamin</option>
                                            <option value="L">Laki-laki</option>
                                            <option value="P">Perempuan</option>
                                        </select>
                                        @error('jenis_kelamin') <span class="text-danger">{{ $message }}</span>@enderror
                    
                                    </div>
                                </div>
                                <!-- Field Tempat Lahir -->
                                <div class="w-full flex flex-row gap-5 my-3 items-center">
                                    <label for="tempat_lahir" class="block text-sm font-medium text-gray-900 w-[100px]">Tempat Lahir</label>
                                    <div class="w-full flex flex-col gap-1">
                                        <input type="text" class="block w-full text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer p-2.5" wire:model="tempat_lahir">
                                            @error('tempat_lahir') <span class="text-danger">{{ $message }}</span>@enderror
                    
                                    </div>
                                </div>
                                <!-- Field Tanggal Lahir -->
                                <div class="w-full flex flex-row gap-5 my-3 items-center">
                                    <label for="tanggal_lahir" class="block text-sm font-medium text-gray-900 w-[100px]">Tanggal Lahir</label>
                                    <div class="w-full flex flex-col gap-1">
                                        <input type="date" class="block w-full text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer p-2.5" wire:model="tanggal_lahir">
                                            @error('tanggal_lahir') <span class="text-danger">{{ $message }}</span>@enderror
                    
                                    </div>
                                </div>
                                <!-- Field Email -->
                                <div class="w-full flex flex-row gap-5 my-3 items-center">
                                    <label for="email" class="block text-sm font-medium text-gray-900 w-[100px]">Email</label>
                                    <div class="w-full flex flex-col gap-1">
                                        <input type="email" class="block w-full text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer p-2.5" wire:model="email">
                                            @error('email') <span class="text-danger">{{ $message }}</span>@enderror
                    
                                    </div>
                                </div>
                                <!-- Field Nomor Telepon -->
                                <div class="w-full flex flex-row gap-5 my-3 items-center">
                                    <label for="no_telepon" class="block text-sm font-medium text-gray-900 w-[100px]">Nomor Telepon</label>
                                    <div class="w-full flex flex-col gap-1">
                                        <input type="text" class="block w-full text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer p-2.5" wire:model="nomor_telepon">
                                            @error('nomor_telepon') <span class="text-danger">{{ $message }}</span>@enderror
                    
                                    </div>
                                </div>
                                <!-- Field Alamat -->
                                <div class="w-full flex flex-row gap-5 my-3 items-center">
                                    <label for="alamat" class="block text-sm font-medium text-gray-900 w-[100px]">Alamat</label>
                                    <div class="w-full flex flex-col gap-1">
                                        <textarea type="text" class="block w-full text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer h-[100px] p-2.5" wire:model="alamat_lengkap"></textarea>
                                            @error('alamat_lengkap') <span class="text-danger">{{ $message }}</span>@enderror
        
                                    </div>
                                </div>
                                <!-- Field Select Picture -->
                                <div class="w-full flex flex-row gap-5 my-3 items-center">
                                    <label class="block text-sm font-medium text-gray-900 w-[100px]" for="file_input">Upload file</label>
                                    <div class="w-full flex flex-col gap-1">
                                        <input class="block w-full text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer p-2.5" id="file_input" type="file" enctype="multipart/form-data" wire:model="foto_profil">
                                        @error('foto_profil') <span class="text-danger">{{ $message }}</span>@enderror
                    
                                    </div>
                                </div>
                                <!-- Tombol Simpan -->
                                <div class="w-full flex flex-col gap-3">
                                    <div class="w-full">
                                        {{-- Tester --}}
                                        {{-- x-on:click="console.log('clicked');" --}}
                                        <button type="button" class="w-full py-3 bg-blue-600 text-white rounded-xl" wire:click='store()'>SIMPAN</button>
                                    </div>
                                    <div class="w-full">
                                        <button type="button" class="w-full py-3 bg-blue-600 text-white rounded-xl" @click="showModalAdd= false">CANCEL</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ModalUpdate -->
            <div x-show="showModalUpdate" x-transition:enter="transition ease-out duration-300 transform" x-transition:enter-start="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95" x-transition:enter-end="opacity-100 translate-y-0 sm:scale-100" x-transition:leave="transition ease-in duration-200 transform" x-transition:leave-start="opacity-100 translate-y-0 sm:scale-100" x-transition:leave-end="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95" class="fixed z-10 inset-0 overflow-y-auto" x-cloak>
                <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
                    <!-- Modal panel -->
                    <div class="w-full inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full" role="dialog" aria-modal="true" aria-labelledby="modal-headline">
                        <div class="my-3 p-3 bg-body rounded shadow-sm">
                            <!-- Formulir untuk Memperbarui Data Mahasiswa -->
                            <form enctype="multipart/form-data">
                                <!-- Field Nama -->
                                <div class="w-full flex flex-row gap-5 my-3 items-center">
                                        <label for="nama" class="block text-sm font-medium text-gray-900 w-[100px]">Nama</label>
                                        <div class="w-full flex flex-col gap-1">
                                            <input type="text" class="block w-full text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer p-2.5" wire:model="nama_lengkap">
                                            @error('nama_lengkap') <span class="text-danger">{{ $message }}</span>@enderror
                                        </div>
                                </div>
                                <!-- Field NIM -->
                                <div class="w-full flex flex-row gap-5 my-3 items-center">
                                        <label for="nim" class="block text-sm font-medium text-gray-900 w-[100px]">NIM</label>
                                        <div class="w-full flex flex-col gap-1">
                                            <input type="text" class="block w-full text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer p-2.5" wire:model="nim">
                                            @error('nim')
                                            @if ($message === 'The nim has already been taken.')
                                                    <span class="text-danger">NIM sudah ada.</span>
                                            @else
                                                    <span class="text-danger">{{ $message }}</span>
                                            @endif
                                            @enderror 
                                        </div>
                                </div>
                                <!-- Field Jenis Kelamin-->
                                <div class="w-full flex flex-row gap-5 my-3 items-center">
                                        <label for="jenis_kelamin" class="block text-sm font-medium text-gray-900 w-[100px]">Jenis Kelamin</label>
                                        <div class="w-full flex flex-col gap-1">
                                            <select id="jenis_kelamin" class="block w-full text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer p-2.5" wire:model="jenis_kelamin">
                                            <option selected value="">Pilih Jenis Kelamin</option>
                                            <option value="L">Laki-laki</option>
                                            <option value="P">Perempuan</option>
                                            </select>
                                            @error('jenis_kelamin') <span class="text-danger">{{ $message }}</span>@enderror
                
                                        </div>
                                </div>
                                <!-- Field Tempat Lahir -->
                                <div class="w-full flex flex-row gap-5 my-3 items-center">
                                        <label for="tempat_lahir" class="block text-sm font-medium text-gray-900 w-[100px]">Tempat Lahir</label>
                                        <div class="w-full flex flex-col gap-1">
                                            <input type="text" class="block w-full text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer p-2.5" wire:model="tempat_lahir">
                                            @error('tempat_lahir') <span class="text-danger">{{ $message }}</span>@enderror
                
                                        </div>
                                </div>
                                <!-- Field Tanggal Lahir -->
                                <div class="w-full flex flex-row gap-5 my-3 items-center">
                                        <label for="tanggal_lahir" class="block text-sm font-medium text-gray-900 w-[100px]">Tanggal Lahir</label>
                                        <div class="w-full flex flex-col gap-1">
                                            <input type="date" class="block w-full text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer p-2.5" wire:model="tanggal_lahir">
                                            @error('tanggal_lahir') <span class="text-danger">{{ $message }}</span>@enderror
                
                                        </div>
                                </div>
                                <!-- Field Email -->
                                <div class="w-full flex flex-row gap-5 my-3 items-center">
                                        <label for="email" class="block text-sm font-medium text-gray-900 w-[100px]">Email</label>
                                        <div class="w-full flex flex-col gap-1">
                                            <input type="email" class="block w-full text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer p-2.5" wire:model="email">
                                            @error('email') <span class="text-danger">{{ $message }}</span>@enderror
                
                                        </div>
                                </div>
                                <!-- Field Nomor Telepon -->
                                <div class="w-full flex flex-row gap-5 my-3 items-center">
                                        <label for="no_telepon" class="block text-sm font-medium text-gray-900 w-[100px]">Nomor Telepon</label>
                                        <div class="w-full flex flex-col gap-1">
                                            <input type="text" class="block w-full text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer p-2.5" wire:model="nomor_telepon">
                                            @error('nomor_telepon') <span class="text-danger">{{ $message }}</span>@enderror
                
                                        </div>
                                </div>
                                <!-- Field Alamat -->
                                <div class="w-full flex flex-row gap-5 my-3 items-center">
                                        <label for="alamat" class="block text-sm font-medium text-gray-900 w-[100px]">Alamat</label>
                                        <div class="w-full flex flex-col gap-1">
                                            <textarea type="text" class="block w-full text-sm text-gray-900 border border-gray-300 h-[100px] rounded-lg cursor-pointer p-2.5" wire:model="alamat_lengkap"></textarea>
                                            @error('alamat_lengkap') <span class="text-danger">{{ $message }}</span>@enderror
                
                                        </div>
                                </div>
                                <!-- Field Select Picture -->
                                <div class="w-full flex flex-row gap-5 my-3 items-center">
                                        <label class="block text-sm font-medium text-gray-900 w-[100px]" for="file_input">Upload file</label>
                                        <div class="w-full flex flex-col gap-1">
                                            <input class="block w-full text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer " id="file_input" type="file" enctype="multipart/form-data" wire:model="foto_profil">
                                            @error('foto_profil') <span class="text-danger">{{ $message }}</span>@enderror
                
                                        </div>
                                </div>
                                <!-- Tombol Simpan -->
                                <div class="w-full flex flex-col gap-3">
                                        <div class="w-full">
                                            <button type="button" class="w-full py-3 bg-blue-600 text-white rounded-xl" name="submit" wire:click.prevent="update()">UPDATE</button>
                                        </div>
                                        <div class="w-full">
                                            <button type="button" class="w-full py-3 bg-blue-600 text-white rounded-xl" @click="showModalUpdate= false">CANCEL</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- Akhir Formulir -->
                    </div>
                    </div>
                </div>
            </div>
    </div>
</div>


@push('styles')
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="https://unpkg.com/flowbite@1.4.4/dist/flowbite.min.css" />
@endpush
@push('scripts')
    <script src="https://cdn.tailwindcss.com"></script>
    <script src="https://unpkg.com/flowbite@1.4.0/dist/flowbite.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/alpinejs@2.8.2"></script>
@endpush